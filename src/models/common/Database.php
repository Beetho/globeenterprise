<?php

class Database extends commonController {
    protected $db;
    
    function __construct() {
        parent::__construct();
        
        $dbConnection = new DB\SQL($this->f3->get('DB_URL'), $this->f3->get('DB_USER'), 
                                   $this->f3->get('DB_PWD'), array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ));
        $this->db = $dbConnection;
    }
    
    public function valida($strEntrada, $intLongitud = 0){
        if($strEntrada == null){
            $strEntrada = "";
            $strEntrada = preg_replace("'", "", $strEntrada);       //  comilla sencilla
            $strEntrada = preg_replace("\n", " ", $strEntrada);     //  \n  newline
            $strEntrada = preg_replace("\t", " ", $strEntrada );    //  \t  tab
            $strEntrada = preg_replace("\r", "", $strEntrada);      //  \r  return
            $strEntrada = preg_replace("\b", "", $strEntrada);      //  \b  backspace
        }
        if(($intLongitud > 0) && (strlen($strEntrada) > $intLongitud)){
            $strEntrada = substr($strEntrada, 0, $intLongitud);
        }        
        return $strEntrada;
    }
    
}