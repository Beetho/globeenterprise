<?php
/**
 * Proyecto Globe
 * Alberto Garcia  <palberto.garcia1010@gmail.com>
 * 2020
 */

class GlobeOBJ extends commonController implements JsonSerializable{
    
    private $idGlobeRegister;
    private $size;
    private $dateRegister;
    
    public function __construct(
        $idGlobeRegister = 0,
        $size            = 0,
        $dateRegister    = ""
        ) {
            $this->idGlobeRegister   = intval ( $idGlobeRegister );
            $this->size              =  $size;
            $this->dateRegister      = $dateRegister;
            
    }
    public function __get($property) {
        if (property_exists ( $this, $property )) {
            return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists ( $this, $property )) {
            $this->$property = $value;
        }
    }
    public function jsonSerialize() {
        return get_object_vars ( $this );
    }
    public function toJson() {
        $objectArray = [ ];
        foreach ( $this as $key => $value ) {
            $objectArray [$key] = $value;
        }
        return json_encode ( $this );
    }
}

