<?php

class GlobeDAO extends Database  {
    private static $instance;
    function __construct() {
        try {
            parent::__construct ();
            self::$instance = $this;
        } catch ( Exception $e ) {
            $this->log->write ( LOG_ERR ."|".__METHOD__."|" . $e );
        }
    }
    public static function instance() {
        if (self::$instance === null) {
            $args = func_get_args ();
            
            if ($args) {
                self::$instance = new self ( args );
            } else {
                self::$instance = new self ();
            }
        }
        return self::$instance;
    }
    
    /**
     * * Funciones PUBLICAS **
     */
    
    
    
    /**
     * save data GlobeOBJ
     *
     * @param $obj GlobeOBJ
     * @return true || false
     * 				Globe Saved
     */
    public function saveSizeGlobe(GlobeOBJ $globe) {
        $this->log->write(LOG_DEBUG."|".__METHOD__."|saveSizeGlobe dao");
        
        $dataSQL = null;
        
        try {
            
            $sql = "INSERT INTO globe
                                (size_globe,
                                date_register)
                        VALUES (:size,
                                :dateRegister)";
            
            $fieldsValues = array(
                ':size'=>$globe->size,
                ':dateRegister'=>$globe->dateRegister
            );
            
            $dataSQL = $this->db->exec ( $sql, $fieldsValues );

            $this->log->write(LOG_DEBUG."|".__METHOD__."|dataSQL " . $dataSQL);
            
            return $dataSQL;
        } catch (Exception $e) {
            $this->log->write ( LOG_ERR ."|".__METHOD__."|Exception|Code: " . $e->getCode () . "|Line: " . $e->getLine () . "|Msg: " . $e->getMessage () );            
            throw new Exception($e->getCode());
        }
    }
    
    
    /**
     * Return last register size globe
     *
     * 
     * @return GlobeOBJ || null
     */
    public function getSizeGlobe() {
        $dataSQL = null;
        $Globe = null;
        
        try {
            $this->log->write(LOG_DEBUG."|".__METHOD__."|getSizeGlobe dao");
            
            $sql = " SELECT   id_globe_register,
                            size_globe,
                            date_register
                    FROM globe
                    WHERE id_globe_register = (SELECT MAX(id_globe_register) FROM globe) ";
            
            $fieldsValues = array();
            
            $dataSQL = $this->db->exec( $sql, $fieldsValues );
            
            
            if ($dataSQL === FALSE) {
                $this->log->write ( LOG_ERR ."|".__METHOD__."|Fallo la consulta" );
            } else if (count ( $dataSQL ) > 0) {
                $this->log->write ( LOG_DEBUG ."|".__METHOD__."|result size: " . count ( $dataSQL ) );
                
                foreach ( $dataSQL as $row ) {
                    $Globe = new GlobeOBJ ();
                    $Globe->idGlobeRegister = $row ['id_globe_register'];
                    $Globe->size = $row ['size_globe'];
                    $Globe->dateRegister = $row ['date_register'];
  
                    $this->log->write ( LOG_DEBUG ."|".__METHOD__."|GlobeOBJ: " . $Globe->toJson () );
                }
            } else {
                $this->log->write ( LOG_DEBUG ."|".__METHOD__."|no existen datos" );
            }
            
            return $Globe;
        } catch (Exception $e) {
            $this->log->write ( LOG_ERR ."|".__METHOD__."|Exception|Code: " . $e->getCode () . "|Line: " . $e->getLine () . "|Msg: " . $e->getMessage () );
            throw new Exception($e->getCode());
        }
    }
    
    
    
    
    
}