<?php
 /**
 * Proyecto Globe
 *
 *  
 **/

include("enumController.php"); 

class commonController {    
    protected $f3;
    protected $log;
    
    function __construct() {    	
        $this->f3 = Base::instance();
        $this->log = new Log($this->f3->get('LOG_FILE'));
        
        $this->log->write(LOG_DEBUG."|".__METHOD__."|Request URI: ".$_SERVER['REQUEST_URI']);

    }
     
}

?>