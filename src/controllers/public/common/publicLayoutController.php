<?php

class publicLayoutController extends commonController {
    
     function __construct() {
    	try {
    		parent::__construct();
    		$this->log->write(LOG_DEBUG."|".__METHOD__."|");
    		
    		if ($this->f3->get('SESSION.logged')) {
    			$this->log->write(LOG_DEBUG."|".__METHOD__."|SESSION.logged: ".$this->f3->get('SESSION.logged'));
    			$this->log->write(LOG_DEBUG."|".__METHOD__."|SESSION.csrf: ".$this->f3->get('SESSION.csrf'));
    		}
    		
    	} catch (Exception $e) {
    		$this->log->write(LOG_DEBUG."|".__METHOD__."|---EXCEPTION--- ".$e->getMessage());
    	}    	
    }
    
    function afterroute() {        
        //Debe ir vacio para actualizar con ajax solo el contenido
        //echo Template::instance()->render('public/common/layout.htm');
    }
    
}
?>