<?php
/**
 * Proyecto 
 *Alberto Garcia <>
 * 
 */
class PublicRestController extends commonController {
    protected $response;
    
    function __construct() {
        
        try {
            parent::__construct();
            
            $this->response = new Response(true, new Metadata(-1, "Invalid request."));
            
            $this->log->write(LOG_DEBUG."|".__METHOD__."|URI: ".$this->f3->get('URI'));
            $this->log->write(LOG_DEBUG."|".__METHOD__."|SESSION.logged: ".$this->f3->get('SESSION.logged'));
            $this->log->write(LOG_DEBUG."|".__METHOD__."|SESSION.csrf: ".$this->f3->get('SESSION.csrf'));
            
            
        } catch (Exception $e) {
            $this->log->write(LOG_ERR."|".__METHOD__."|Exception: ". $e->getLine()."|".$e->getMessage());
        }
    }
    
}