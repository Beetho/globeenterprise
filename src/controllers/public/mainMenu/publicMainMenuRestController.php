<?php


class publicMainMenuRestController extends publicRestController {
    
    function __construct() {
        try {
            parent::__construct();
            $this->log->write(LOG_DEBUG."|".__METHOD__."|");
           // $this->log->write(LOG_DEBUG."|".__METHOD__."|".$this->f3->get('HOST')."|".$this->f3->get('IP')."|".$this->f3->get('AGENT')."|isDesktop: ".$this->audit->isdesktop()."|isMobile:".$this->audit->ismobile()."|isBot:".$this->audit->isbot());
            
            
        } catch (Exception $e) {
            $this->log->write(LOG_DEBUG."|".__METHOD__."|ERROR: Exception ".$e->getMessage());
        }
    }
    
    
    public function saveSizeCircle(){
        try {
            $this->log->write(LOG_DEBUG."|".__METHOD__."|START");
            $globeDAO = GlobeDAO::instance();
            $sizeCircle = $this->f3->get('POST.sizeGlobe');
            $this->log->write(LOG_DEBUG."|".__METHOD__."|sizeCircle: ".$sizeCircle);
            
            if($sizeCircle != null && $sizeCircle > 0){
                if(is_numeric($sizeCircle)){
                    $this->log->write(LOG_DEBUG.__METHOD__."|es numero");
                    $globe = new GlobeOBJ();
                    $globe->size = $sizeCircle;
                    $globe->dateRegister = date('Y-m-d H:i:s');
                    
                    $resultSaveData = $globeDAO->saveSizeGlobe($globe);
                    if($resultSaveData){
                        $this->log->write(LOG_DEBUG.__METHOD__."|ErrorMsg: no se pudo guardar el registro");
                        
                        $this->response->success = true;
                        $this->response->metadata->id = 1;
                        $this->response->metadata->msg = "Los datos se guardaron correctamente";
                        
                        $jsonOut = $this->response->toJson();
                    }else{
                        $this->log->write(LOG_DEBUG.__METHOD__."|ErrorMsg: no se pudo guardar el registro");
                        
                        $this->response->success = true;
                        $this->response->metadata->id = -3;
                        $this->response->metadata->msg = "No se pudo guardar su registro, favor de volver a intentar";
                        
                        $jsonOut = $this->response->toJson();
                    }
                }
            }else{
                $this->log->write(LOG_DEBUG.__METHOD__."|ErrorMsg: el campo esta vacio");
                
                $this->response->success = true;
                $this->response->metadata->id = -2;
                $this->response->metadata->msg = "Debes escribir un dato valido";
                
                $jsonOut = $this->response->toJson();
            }
            
            $this->log->write(LOG_DEBUG."|".__METHOD__."|END");
        } catch (Exception $e){
            $this->log->write(LOG_DEBUG."|".__METHOD__."|ERROR: Exception ".$e->getMessage());
            
            $this->response->success = true;
            $this->response->metadata->id = -99;
            $this->response->metadata->msg = "Error de Sistema: Contactar a administrador.";
        }
        
        header('Content-Type: application/json, charset=UTF-8');
        $this->log->write(LOG_DEBUG."|".__METHOD__."|Output:".$jsonOut);
        echo $jsonOut;
    }
    
    
    public function getDataGlobe(){
        $this->log->write(LOG_DEBUG."|".__METHOD__."|CALL");
        $globeDAO = GlobeDAO::instance();
        
        $Globe = $globeDAO->getSizeGlobe();
        
        if($Globe->idGlobeRegister != null ){
            $this->response->success = true;
            $this->response->metadata->id = 1;
            $this->response->metadata->msg = "Datos Correctos";
            $data['dataGlobe'] = $Globe;
            
            $this->response->data = $data;
        }else{
            $this->log->write(LOG_DEBUG.__METHOD__."|no se encontraron datos");
            
            $this->response->success = true;
            $this->response->metadata->id = 2;
            $this->response->metadata->msg = "No se encontraron datos";
        }
        
        $result = $this->response->toJson();
        $this->log->write(LOG_DEBUG.__METHOD__."|".$result);
        header('Content-Type: application/json, charset=UTF-8');
        echo $result;
    }
    
}