<?php

class publicMainMenuController extends publicLayoutController {
	
	function __construct() {
		try {
			parent::__construct();
			$this->log->write(LOG_DEBUG."|".__METHOD__."|");
			
		} catch (Exception $e) {
			$this->log->write(LOG_DEBUG."|".__METHOD__."|---EXCEPTION--- ".$e->getMessage());
		}
	}
	
	function showConfig(){
	    try {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|START");
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|IP blackedlisted: ".$this->f3->get('IP')."-".$this->f3->blacklisted($this->f3->get('IP')));
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|Ajax:".$this->f3->get("AJAX"));
	        
	        $this->f3->set('PAGE', 'Config');

	        if($this->f3->get("AJAX") == 1){
	            echo Template::instance()->render('public/config.html');
	        }else{
	            $this->f3->set('container','public/config.html');
	            echo Template::instance()->render('public/common/layout.htm');
	        }
	        
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|END");
	    } catch (Exception $e) {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|ERROR: Exception ".$e->getMessage());
	    }
	}
	
	
	function showGlobe(){
	    try {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|START");
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|IP blackedlisted: ".$this->f3->get('IP')."-".$this->f3->blacklisted($this->f3->get('IP')));
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|Ajax:".$this->f3->get("AJAX"));
	        
	        $this->f3->set('PAGE', 'Globe');
	        
	        if($this->f3->get("AJAX") == 1){
	            echo Template::instance()->render('private/globe.html');
	        }else{
	            $this->f3->set('container','private/globe.html');
	            echo Template::instance()->render('private/common/layout.htm');
	        }
	        
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|END");
	    } catch (Exception $e) {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|ERROR: Exception ".$e->getMessage());
	    }
	}
	
	
	function showWebSocketGlobe(){
	    try {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|START");
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|IP blackedlisted: ".$this->f3->get('IP')."-".$this->f3->blacklisted($this->f3->get('IP')));
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|Ajax:".$this->f3->get("AJAX"));
	        
	        $this->f3->set('PAGE', 'Globe Web Socket');
	        $this->f3->set('SOCKET_FRONT', $this->f3->get('SOCKET'));
	        $this->f3->set('SOCKET_BACKEND_IP', $this->f3->get('DOMAIN'));
	        $this->f3->set('SOCKET_BACKEND_PORT', $this->f3->get('PORT'));
	        
	        if($this->f3->get("AJAX") == 1){
	            echo Template::instance()->render('private/wsglobe.html');
	        }else{
	            $this->f3->set('container','private/wsglobe.html');
	            echo Template::instance()->render('private/common/wslayout.htm');
	        }
	        
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|END");
	    } catch (Exception $e) {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|ERROR: Exception ".$e->getMessage());
	    }
	}
	
	
	function showWebSocketConfig(){
	    try {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|START");
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|IP blackedlisted: ".$this->f3->get('IP')."-".$this->f3->blacklisted($this->f3->get('IP')));
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|Ajax:".$this->f3->get("AJAX"));
	        
	        $this->f3->set('PAGE', 'Globe Web Socket');
	        $this->f3->set('SOCKET_FRONT', $this->f3->get('SOCKET'));
	        $this->f3->set('SOCKET_BACKEND_IP', $this->f3->get('DOMAIN'));
	        $this->f3->set('SOCKET_BACKEND_PORT', $this->f3->get('PORT'));
	        
	        if($this->f3->get("AJAX") == 1){
	            echo Template::instance()->render('public/wsconfig.html');
	        }else{
	            $this->f3->set('container','public/wsconfig.html');
	            echo Template::instance()->render('private/common/wslayout.htm');
	        }
	        
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|END");
	    } catch (Exception $e) {
	        $this->log->write(LOG_DEBUG."|".__METHOD__."|ERROR: Exception ".$e->getMessage());
	    }
	}
	
	
}
