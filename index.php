<?php

require "vendor/autoload.php";
$f3 = Base::instance();

// Setup environment
$f3->config('config/setup.cfg');
if($f3->get('ENV') == 'DEV'){           // DEVELOPMENT
	// PHP Error Reporting
	error_reporting( E_ALL );
	ini_set( "display_errors", true );
	ini_set( "display_startup_errors", true );
	
	$f3->config('config/development.cfg');
}else if($f3->get('ENV') == 'PROD'){    // PRODUCTION
	$f3->config('config/production.cfg');
}


if ($f3->exists('HTTPS_ACTIVE')) {
	if ($f3->get('HTTPS_ACTIVE')) {
		$f3->set('APP_PROTOCOL', 'https://');
	} else {
		$f3->set('APP_PROTOCOL', 'http://');
	}
} else {
	$f3->set('APP_PROTOCOL', 'http://');
}


// PHYSICAL PATHS
$f3->set('PHYSICAL_APP_PATH', $f3->get('PHYSICAL_BASE_PATH').$f3->get('PHYSICAL_APP_DIR'));
$f3->set('PHYSICAL_HOME_PATH', $f3->get('PHYSICAL_APP_PATH').$f3->get('PHYSICAL_PUBLIC_DIR'));
$f3->set('LOGS', $f3->get('PHYSICAL_APP_PATH').$f3->get('LOGS_DIR'));
$f3->set('TEMP',$f3->get('PHYSICAL_APP_PATH').$f3->get('TEMP_DIR'));
$f3->set('IMG_PATH',$f3->get('PHYSICAL_APP_PATH').$f3->get('IMG_DIR'));

// URL PATHS
if ( $f3->exists('SUBDOMAIN') && $f3->get('SUBDOMAIN') ) {
	$f3->set('HTTP_DOMAIN', $f3->get('APP_PROTOCOL').$f3->get('SUBDOMAIN').'.'.$f3->get('DOMAIN'));
}else{
	$f3->set('HTTP_DOMAIN', $f3->get('APP_PROTOCOL').$f3->get('DOMAIN'));
}
$f3->set('HTTP_HOME',  $f3->get('HTTP_DOMAIN').$f3->get('HTTP_CONTEXT_PATH'));
$f3->set('HTTP_IMG_PATH', $f3->get('HTTP_HOME').$f3->get('IMG_DIR'));


// Autoload Controllers
$f3->set('AUTOLOAD', '
				src/controllers/;
				src/controllers/common/;
				src/controllers/public/common/;
				src/controllers/public/mainMenu/;
				src/controllers/private/common/;
				src/controllers/private/mainMenu/;
				src/controllers/private/reportesMenu/;
				src/controllers/private/adminMenu/;
                src/controllers/private/ajustesMenu/;
                src/controllers/private/datosMenu/;
				src/controllers/tables/;
				src/controllers/tables/common/;
                src/models/;
				src/models/common/;
				src/models/dataobjects/;
				src/models/objects/;


        ');


// Routes to URL calls
$f3->config('config/routes.cfg');

$f3->set('ONERROR',	function($f3) {
	$log = new Log($f3->get('LOG_ERROR_FILE'));
	$errorMsg = 'Title: '.$f3->get('ERROR.title').'|Desc: '.$f3->get('ERROR.text').'|Trace|'.$f3->get('ERROR.trace');
	
	$log->write(LOG_DEBUG."|".__METHOD__."|---ERROR---|".$f3->get('ERROR.code'));
	
	if($f3->get("AJAX") == 1){
		$log->write(LOG_DEBUG."|Ajax|".$errorMsg);
	}else{
		$log->write(LOG_DEBUG."|".$errorMsg);
	}
	
	if($f3->get('ENV') == 'DEV'){
		echo "<div style='color:red'><h1>---ERROR----</h1>".__METHOD__."<br>".str_replace("|", "<br>", str_replace("()","()<br>",$errorMsg)."</div>");
	}
});    


// Run
$f3->run();

