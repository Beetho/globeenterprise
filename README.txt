Requerimientos:
	- PHP >=7.2
	- MySQL (MariaDB 10.4.17)

Base de Datos:
	El script para la creaci�n de la DB y sus tablas se encuentran
	en la siguiente ubicaci�n:  Globe\documentation\BD\script_globe_db.sql
	solo basta con ejecutarlo


Configuraci�n de ambiente:
	En �sta ubicaci�n: Globe\config\setup.cfg es donde se configura el ambiente 
	en el que vamos a trabajar, para esta ocasi�n ser� el ambiente de 
	desarrollo, por lo tanto, ubicaremos la siguiente linea de c�digo:
	ENV="DEV" y colocaremos "DEV" en caso de tener otro.
	"DEV" -> Desarrollo
	"PROD" -> Producci�n

Configuraci�n ambiente DEV:
	En Globe\config\development.cfg es donde configuramos nuestras varibales
	de entorno:
		1) ubicamos la linea: DOMAIN="192.168.0.5", aqu� colocaremos 
			nuestra IP, para obtenerla basta con ejecutar: ipconfig
			desde nuestra consola (cmd), o en su defecto 
			colocar DOMAIN="localhost"
		2) ubicamos las siguientes lineas:
			PHYSICAL_BASE_PATH="C:/xampp/htdocs/" ---> colocamos la ubicaci�n donde se aloja nuestro proyecto
			PHYSICAL_APP_DIR="Globe/"             ---> colocamos el nombre con el que guardamos nuestro proyecto
			HTTP_CONTEXT_PATH="/Globe/"           ---> colocamos el nombre con el que guardamos nuestro proyecto

			DB_URL="mysql:host=localhost;port=3306;dbname=globe_db" ---> colocamos los datos correspondientes a nuestra DB
			DB_USER="root"						---> colocamos el usuario de nuestra DB
			DB_PWD=""						---> colocamos la contrase�a de nuestra DB


Pantallas en el navegador:
	Para visualizar la pantalla donde ingresaremos el tama�o de nuestro Globo
	escribimos en nuestro navegador: http://localhost/Globe/config/globe � http://192.168.0.5/Globe/config/globe

	Para visualizar la pantalla donde se muestra nuestro Globo
	escribimos en nuestro navegador: http://localhost/Globe/globe � http://192.168.0.5/Globe/globe

	Nota: El tama�o que escribamos, ser� reflejado en el 
		  
	


