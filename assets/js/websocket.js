
var Server;

$(document).ready(function(){
	Server = new FancyWebSocket('ws://'+urlPort);
	Server.bind('open',function()
			{
				//
			});
	Server.bind('close',function( data )
			{
				//
			});
	Server.bind('message',function( payload )
			{
				//
			});
	Server.connect();
});

function send(text){
	//console.log("text: "+text);
	Server.send('message',text);
}


var FancyWebSocket = function(url)
{
	var callbacks = {};
	var ws_url = url;
	var conn;

	this.bind = function(event_name, callback){
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;// chainable
	};

	this.send = function(event_name, event_data){
		//console.log("event_data: "+event_data);
		this.conn.send( event_data );
		return this;
	};

	this.connect = function() {
		if ( typeof(MozWebSocket) == 'function' )
			this.conn = new MozWebSocket(url);
		else
			this.conn = new WebSocket(url);

		// dispatch to the right handlers
		this.conn.onmessage = function(evt){
			dispatch('message', evt.data);
		};

		this.conn.onclose = function(){dispatch('close',null)}
		this.conn.onopen = function(){dispatch('open',null)}
	};

	this.disconnect = function() {
		this.conn.close();
	};

	var dispatch = function(event_name, message){
		//console.log("message: "+message);

		var msg = JSON.parse(message);

		if(!isConfig && msg != null){
			//console.log("msg: "+msg);

			$('#divCircle').show();
			$('#title').text("Globe Size: "+msg.sizeGlobe+" px");

			$('#wscircleGlobeSVG').attr('width',msg.sizeGlobe);
			$('#wscircleGlobeSVG').attr('height',msg.sizeGlobe);
			
			$('#wscircleGlobe').attr('r',msg.r);
			$('#wscircleGlobe').attr('cx',msg.cx);
			$('#wscircleGlobe').attr('cy',msg.cy);
			
		}

	}
};