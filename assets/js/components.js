//ENUM
const RESPUESTA = {
		OK:1,
		NODATA:-2,
		ERROR:3
}
const REGSTATUS = {
	BORRADA: 4,
	EXISTENTE: 3
}
const VALIDATE = {
	OK: 1,
	NOK: 0
}

function addMethod(object, name, fn){
    var old = object[ name ];
    object[ name ] = function(){
        if ( fn.length == arguments.length )
             return fn.apply( this, arguments );
         else if ( typeof old == 'function' )
            return old.apply( this, arguments );
     };
}

function openUrl(objUrl){
	if(objUrl.length > 0){
		$("#waitingPopUp").ejWaitingPopup('show');

		window.history.replaceState('', '', objUrl);
		// alert("URL" +objUrl);
		console.log('URL CALL: ' + objUrl);
		$("#mainContainer").load(objUrl, function(){
			$("#waitingPopUp").ejWaitingPopup('hide');
		});
	} 
}


function openUrlAjax(objDiv, objUrl){
	if(objUrl.length > 0){
		$("#waitingPopUp").ejWaitingPopup('show');

		// alert("DIV "+ objDiv + " URL " +objUrl);
		console.log('URL CALL: ' + objUrl);
		$("#"+objDiv).load(objUrl, function(){
			$("#waitingPopUp").ejWaitingPopup('hide');
		});
	} 
}

// Usage: console.log(padLeft(23,5));  --> 00023
// console.log(padLeft(23,5,'>>')); -----> >>>>>>23
 function padLeft(numero, longitud, strToPad){
    return Array(longitud-String(numero).length+1).join(strToPad||'0')+numero;
}

 function drawMap(objMap, objLatitud, objLongitud, objZoom, valueLatitud, valueLongitud, valueZoom){

		var latBD = parseFloat(valueLatitud);
		var lngBD = parseFloat(valueLongitud);
		var zoomBD = parseInt(valueZoom);
		var latMap = latBD || 19.4326077;
		var lngMap = lngBD || -99.13320799999997;
		var zoomMap = zoomBD || 5;
		
		var map = new google.maps.Map(document.getElementById('mapa'), {
	   	 
	        zoom: zoomMap,
		    center: {lat: latMap, lng: lngMap},
		    mapTypeControl: true,
	        mapTypeControlOptions: {
	            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
	            mapTypeIds: ['roadmap', 'terrain']
	        }

	      });
	      var input = document.getElementById('buscarMap');
	      var searchBox = new google.maps.places.SearchBox(input);
	      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	      var markers = [];
	 
	      var marker = new google.maps.Marker({
	        position: {lat: latMap, lng: lngMap},//---- cambie lat:-25.363, lng: 131.044 por los valores de BD
	        map: map,
	        draggable:false,
	        animation:google.maps.Animation.DROP,
	      });

	      
	  

	}




function drawMapSearch(objMap, objSearch, objLatitud, objLongitud, objZoom, valueLatitud, valueLongitud, valueZoom){

	var latBD = parseFloat(valueLatitud);
	var lngBD = parseFloat(valueLongitud);
	var zoomBD = parseInt(valueZoom);
	var latMap = latBD || 19.4326077;
	var lngMap = lngBD || -99.13320799999997;
	var zoomMap = zoomBD || 5;
	
	var map = new google.maps.Map(document.getElementById('mapa'), {
   	 
        zoom: zoomMap,
	    center: {lat: latMap, lng: lngMap},
	    mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            mapTypeIds: ['roadmap', 'terrain']
        }

      });
      var input = document.getElementById('buscarMap');
      var searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      var markers = [];
      searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          var bounds = new google.maps.LatLngBounds();
          bounds.extend(places[0].geometry.location);
          map.fitBounds(bounds);
          map.setZoom(17);//--------- CAMBIE 17 por zoomBD
          marker.setPosition(places[0].geometry.location);
      	 var pos = marker.getPosition();
	            console.log(pos.lat());
	            console.log(pos.lng());
	            console.log(map.getZoom());
	            
	           /* 
	            $('#latitudShow').val(pos.lat());
	            $('#longitudShow').val(pos.lng());
	            $('#zoomShow').val(map.getZoom());
	            $('#latitudUpdate').val(pos.lat());
	            $('#longitudUpdate').val(pos.lng());
	            $('#zoomUpdate').val(map.getZoom());*/
				
	            $('#' + objLatitud).val(pos.lat());
	            $('#' + objLongitud).val(pos.lng());
	            $('#' + objZoom).val(map.getZoom());
	            
        });
 
      var marker = new google.maps.Marker({
        position: {lat: latMap, lng: lngMap},//---- cambie lat:-25.363, lng: 131.044 por los valores de BD
        map: map,
        draggable:true,
        animation:google.maps.Animation.DROP,
      });
      map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
          console.log("entro Maker");
        });
      
      google.maps.event.addListener(map,'click',function(event){
      	var result = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
      	marker.setPosition(result);
      	 var pos = marker.getPosition();
	            console.log(pos.lat());
	            console.log(pos.lng());
	            console.log(map.getZoom());
	            
	           /* $('#latitudShow').val(pos.lat());
	            $('#longitudShow').val(pos.lng());
	            $('#zoomShow').val(map.getZoom());
	            $('#latitudUpdate').val(pos.lat());
	            $('#longitudUpdate').val(pos.lng());
	            $('#zoomUpdate').val(map.getZoom());*/
	            
	            $('#' + objLatitud).val(pos.lat());
	            $('#' + objLongitud).val(pos.lng());
	            $('#' + objZoom).val(map.getZoom());
      });
      
     
      google.maps.event.addListener(marker, 'drag', function() {
          console.log('Dragging...');
          
          var pos = marker.getPosition();
          console.log(pos.lat());
          console.log(pos.lng());
          console.log(map.getZoom());
          
          /*$('#latitudShow').val(pos.lat());
          $('#longitudShow').val(pos.lng());
          $('#zoomShow').val(map.getZoom());
          $('#latitudUpdate').val(pos.lat());
          $('#longitudUpdate').val(pos.lng());
          $('#zoomUpdate').val(map.getZoom());*/
          
          $('#' + objLatitud).val(pos.lat());
          $('#' + objLongitud).val(pos.lng());
          $('#' + objZoom).val(map.getZoom());
      });
	
}

function getUrlData(pathUrl){ //funciÃ³n genÃ©rica para traer datos del servidor
	 //var resultado;
		$("#wait").ejWaitingPopup({ showOnInit: true, appendTo:".main"});
			var dataManager = ej.DataManager({
				
			    url: pathUrl,
			    headers: [{ Token: "{{@SESSION.csrf}}" }]
			    });

			var dataSource = dataManager.executeQuery(ej.Query()).done(function (args) {
			$("#wait").ejWaitingPopup('hide');
			console.log(args.result);
			resultado = args.result;

			});

		return resultado;

	}

function getUrlDataAjax(pathUrl){ //función genérica para traer datos del servidor
	
	 var resultado;
	 //console.log("token components "+token);

	 $("#waitingPopUp").ejWaitingPopup('show');
	 $.ajax({
			url: pathUrl,
			//headers: [{ Token: "{{@SESSION.csrf}}" }],
			async: false,
			type: 'GET',
			dataType: 'json',
			beforeSend: function(request){
				// request.setRequestHeader('Token', token);
			},
			success: function(response){
				//console.log(response);
				resultado = response;

			},
			error: function(request, errorInfo){
				console.log(errorInfo);
			}
		});
		$("#waitingPopUp").ejWaitingPopup('hide');
		return resultado;
	}

function postUrlDataAjax(pathUrl, form, token){ //función genérica para guardar datos del servidor
	var resultado;
	var params = $("#" + form).serializeArray();
	var formData = new FormData();

	if ($("#" + form).find('[name="archivo"]').length > 0) {
		var files = $("#" + form).find('[name="archivo"]')[0].files;
		$.each(files, function(i, file) {
			formData.append('archivo', file);
		});
	}

	$.each(params, function(i, val) {
		formData.append(val.name, val.value);
	});
	 //var postdata = $("#"+form).serialize();
	 $.ajax({
			url: pathUrl,
			async: false,
			type: 'POST',
			dataType: 'json',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(request){
				request.setRequestHeader('Token', token);
			},
			success: function(response){
				console.log(response);
				resultado = response;

			},
			error: function(request, error){
				console.log(error);
			}
		});
		return resultado;
	}


function block(){
		
	addMethod(this,"Titulo", function(idElement, objTitle, objSubtitle, objIcon){
		objSubtitle = objSubtitle || "";	
		objIcon = objIcon || "";	
		
		var htmlCode = '<h1 class="styleTitle">'
			htmlCode += (objIcon == "")?"":'<span class="fas fa-'+objIcon+' fa-fw"></span>&nbsp;';
			htmlCode += objTitle+' <small><small>'+objSubtitle+'</small></small></h1>';
			
			// console.log(htmlCode);
			
		$('#' + idElement).append(htmlCode);
		
	});
		
	
	addMethod(this, "Hidden", function(idElement, objName, objValue){		
		objValue = objValue || "";		

		var htmlCode = '<input type="HIDDEN"  id="'+objName+'" name="'+objName+'" value="'+objValue+'"> ';
		
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);
	});

	addMethod(this, "Text", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objLength){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "keyboard";
		objLength = objLength || "100";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text"  id="'+objName+'" name="'+objName+'" maxlength="'+objLength+'" ';
			
			if(objRequired == 1){
	        	//htmlCode += " required ";
	        	htmlCode += "";
	        } 
			
			// htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ></div>';			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejMaskEdit({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' inputMode: ej.InputMode.Text, '+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this, "TextArea", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "keyboard";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control" style="height:auto;">';
			htmlCode += '<textarea rows="3" class="form-control e-textbox" id="'+objName+'" name="'+objName+'" '+
				' style="resize: none;width:98%;" placeholder="'+objPlaceHolder+'" ';
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			
			// htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' >'+objValue+'</textarea></div>';		
			
			/*
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejMaskEdit({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' inputMode: ej.InputMode.Text, '+
			     		' value:"'+objValue+'" });</script>';
			     		*/
        } else {
        	htmlCode += '<textarea rows="3" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" style="resize:none;" ';
        	htmlCode += " READONLY ";
			htmlCode += ' >'+objValue+'</textarea>';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	
	// https://help.syncfusion.com/api/js/ejmaskedit#members:maskformat
	addMethod(this, "TextMask", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objMask){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "keyboard";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text"  id="'+objName+'" name="'+objName+'" ';
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			
			// htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ></div>';			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejMaskEdit({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' inputMode: ej.InputMode.Text, '+
				     	' maskFormat: "'+objMask+'", '+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        	
        	htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").mask(\''+objMask+'\');</script>';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this, "Password", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "lock";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text"  id="'+objName+'" name="'+objName+'" ';
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 

			htmlCode += " onkeypress=\"Valida(event,'allnospace',this.value);\" ";
			
			// htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ></div>';		
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejMaskEdit({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' inputMode: ej.InputMode.Password, '+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this, "Numeric", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objDecimals){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "calculator";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	//htmlCode += " required ";
	        	htmlCode += "";
	        } 
			
			htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' </div>';
			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejNumericTextbox({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' showSpinButton: false, '+
			     		' decimalPlaces: '+objDecimals+','+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this, "Money", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objDecimals){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "dollar-sign";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			
			htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ></div>';
			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejCurrencyTextbox({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' showSpinButton: false, positivePattern:"$ n",'+
			     		' decimalPlaces: '+objDecimals+','+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	

	addMethod(this, "Percent", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objDecimals){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "percentage";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			
			htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ></div>';
			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejPercentageTextbox({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' showSpinButton: false, '+
			     		' decimalPlaces: '+objDecimals+','+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	

	addMethod(this, "Email", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "envelope";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text"  id="'+objName+'" name="'+objName+'" ';
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 

			htmlCode += " onkeypress=\"Valida(event,'email',this.value);\" ";
			htmlCode += " onchange=\"CheckEmail('"+objName+"');\" ";
			
			// htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ></div>';			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejMaskEdit({ locale: "es-MX", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' inputMode: ej.InputMode.Text, '+
			     		' value:"'+objValue+'" });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	
	addMethod(this, "Date", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "calendar-alt";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		
		if(objReadonly == 0){
			
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			
			htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' </div>';
			
			
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejDatePicker({ locale: "es-MX", '+
			     		' format: "dd/MM/yyyy", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' value:new Date(\"'+objValue+'\"), '+
						' buttonText: "Hoy"});</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this, "DateTime", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "calendar-day";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		

		if(objReadonly == 0){
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			htmlCode += ' ></div>';
		
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejDateTimePicker({ locale: "es-MX", '+
			     		' format: "dd/MM/yyyy hh:mm", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' value:new Date(\"'+objValue+'\"), '+
						' buttonText: { today: "hoy", timeNow: "ahora", done: "listo", timeTitle: "tiempo" } });</script>';
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	

	addMethod(this, "DateRange", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "calendar-week";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
		
		if(objReadonly == 0){
			htmlCode += 	'<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			htmlCode += ' ></div>';
		
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejDateRangePicker({ locale: "es-MX", '+
			     		' format: "dd/MM/yyyy", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' height:"initial", width:"100%", '+
			     		' value:"'+objValue+'" '+
			     		'});</script>';

        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	
	addMethod(this, "Combo", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, urlDataSource, dsId, dsValue){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "list";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		

		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
	
		if(objReadonly == 0){
			
			
			htmlCode += '<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
	        	
			htmlCode += ' ></div>';

			var dataUrl = getUrlDataAjax(urlDataSource);
			var objDataSource = (dataUrl.data != null ? dataUrl.data.gridData : "" ) ;
			// console.log(dataUrl);
			console.log(objDataSource);
				
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

		if(objReadonly == 0){
			$('#' + objName).ejDropDownList({		 	
			 	dataSource: objDataSource,
			 	height:"initial", width:"100%",
				watermarkText: objPlaceHolder,
	     		fields: { id: dsId, text: dsValue, value: dsId }
			});
			
			if (objValue.length>0){
				$('#'+objName).ejDropDownList("selectItemByText",objValue);
			}
			
			if(objHelp.length > 0 ){
				$('#'+objName+'ToolTip').ejTooltip({content: objHelp, 
					position :{  stem: { horizontal: "right", vertical: "bottom" }, 
					target: { horizontal : "left", vertical : "center" } } 
				});
			}
		}
	});
	
	
	addMethod(this, "ComboPlus", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, urlDataSource, dsId, dsValue,permission){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "list";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		

		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
	
		if(objReadonly == 0){
			
			
			htmlCode += '<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
	        	
			htmlCode += ' ></div>';

			var dataUrl = getUrlDataAjax(urlDataSource);
			var objDataSource = (dataUrl.data != null ? dataUrl.data.gridData : "" ) ;
			// console.log(dataUrl);
			console.log(objDataSource);
				
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(permission > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-plus fa-fw"></span></span>'+
						'</div>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

		if(objReadonly == 0){
			$('#' + objName).ejDropDownList({		 	
			 	dataSource: objDataSource,
			 	height:"initial", width:"100%",
				watermarkText: objPlaceHolder,
	     		fields: { id: dsId, text: dsValue, value: dsId }
			});
			
			if (objValue.length>0){
				$('#'+objName).ejDropDownList("selectItemByText",objValue);
			}
			
			if(objHelp.length > 0 ){
				$('#'+objName+'ToolTip').ejTooltip({content: objHelp, 
					position :{  stem: { horizontal: "right", vertical: "bottom" }, 
					target: { horizontal : "left", vertical : "center" } } 
				});
			}
		}
	});
	
	
	
	addMethod(this, "ComboObj", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objDataSource, dsId, dsValue){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "list";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		

		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
	
		if(objReadonly == 0){
			
			
			htmlCode += '<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
	        	
			htmlCode += ' ></div>';

        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

		if(objReadonly == 0){
			$('#' + objName).ejDropDownList({		 	
			 	dataSource: objDataSource,
			 	height:"initial", width:"100%",
				watermarkText: objPlaceHolder,
	     		fields: { id: dsId, text: dsValue, value: dsId }
			});
			
			if (objValue.length>0){
				$('#'+objName).ejDropDownList("selectItemByText",objValue);
			}
			
			if(objHelp.length > 0 ){
				$('#'+objName+'ToolTip').ejTooltip({content: objHelp, 
					position :{  stem: { horizontal: "right", vertical: "bottom" }, 
					target: { horizontal : "left", vertical : "center" } } 
				});
			}
		}
	});
	
	
	addMethod(this, "ComboImage", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, urlDataSource, objTemplate,base){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "list";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		

		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';		
	
		if(objReadonly == 0){
			
			
			htmlCode += '<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
	        	
			htmlCode += ' ></div>';

			var dataUrl = getUrlDataAjax(urlDataSource);
			var objDataSource = (dataUrl.data != null ? dataUrl.data.gridData : "" ) ;
			// console.log(dataUrl);
			console.log(objDataSource);
				
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

		if(objReadonly == 0){
			$('#' + objName).ejDropDownList({		 	
			 	height:"initial", width:"100%",
				watermarkText: objPlaceHolder
			});

			var txt = [];
			var srs;
			for(var i=0;i<objDataSource.length;i++){
				var valor = objDataSource[i].valorText;
				var subTxt = valor.split('.');
				srs = {valor:subTxt[0]}
				txt.push(srs);
			}
			console.log("xxxx "+txt[1].valor);
				$('#' + objName).ejDropDownList({
				 	dataSource: objDataSource,
					template:"<div><img class='eimg' src='"+base+"/assets/images/${valorText}' alt='status' />"+
                    "<div class='nombre'> ${nombre} </div></div>"
				});
			
			
			if (objValue.length>0){
				$('#'+objName).ejDropDownList("selectItemByText",objValue);
			}
			
			if(objHelp.length > 0 ){
				$('#'+objName+'ToolTip').ejTooltip({content: objHelp, 
					position :{  stem: { horizontal: "right", vertical: "bottom" }, 
					target: { horizontal : "left", vertical : "center" } } 
				});
			}
		}
	});
	
	

	addMethod(this, "Autocomplete", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp, objDataSource, dsId, dsValue){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "list";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		

		htmlCode += 	'<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';
		
		if(objReadonly == 0){		
			
			htmlCode += '<div class="form-control">';
			htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'"  '
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			if(objReadonly == 1){
	        	htmlCode += " READONLY ";
	        } 
	        	
			
			htmlCode += ' ></div>';
		
			htmlCode += '<script type="text/javascript">';
			htmlCode +=  '$("#'+objName+'").ejAutocomplete({ dataSource: ['+objDataSource+'], '+
			     		' height:"initial", width:"100%", '+
			     		' watermarkText: "'+objPlaceHolder+'", '+
			     		' filterType: "contains", highlightSearch: true,'+
			     		' showPopupButton: true, '+
			     		' fields: { id: "'+dsId+'", text: "'+dsValue+'", value: "'+dsValue+'" } });';
			if (objValue.length>0){
				htmlCode +=  '$("#'+objName+'").ejAutocomplete("selectValueByText", "'+objValue+'");'; 
			}
			 htmlCode += ' </script>';	
			
			

	        $('#dropdown').ejDropDownList("selectItemByValue", "ComputerIT, Cookery ");
			
			
        } else {
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" '+
			'placeholder="'+objPlaceHolder+'" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objValue+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	

	addMethod(this, "Check", function(idElement, objName, objTitle, objPlaceHolder, objValue, objRequired, objReadonly, objIcon, objHelp){
		objTitle = objTitle || "";
		objPlaceHolder = objPlaceHolder || "";
		objValue = objValue || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "square";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		if(objReadonly == 0){
			if ((objIcon == "square") && (objValue == 1)) {
				htmlCode += '<div class="input-group-prepend" ">'+
						'<span class="input-group-text" ><span class="far fa-check-square fa-fw"></span></span>'+
						'</div>';	
			} else{
				htmlCode += '<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
					'</div>';
			}
			
			htmlCode += 	'<div class="form-control">';
			
			htmlCode += '<input type="checkbox" class="checkbox" id="'+objName+'" name="'+objName+'" ';
			
			if(objRequired == 1){
	        	htmlCode += " required ";
	        } 
			
			// htmlCode += 'value="'+objValue+'" >';
			htmlCode += ' ><label for="'+objName+'">&nbsp;'+objPlaceHolder+'</label></div>';	
			
			
			
			htmlCode += '</div>';
			
			htmlCode += '<script type="text/javascript">';
				htmlCode +=  '$("#'+objName+'").ejCheckBox({ ';
				if (objValue==1) {
					htmlCode +=  ' checked: true ';
				}
				htmlCode +=  ' });'
			htmlCode +=  ' </script>';
        } else {
        	if (objValue == 1) {
				htmlCode += '<div class="input-group-prepend" ">'+
						'<span class="input-group-text" ><span class="far fa-check-square fa-fw"></span></span>'+
						'</div>';	
        	} else{
        		htmlCode += '<div class="input-group-prepend" ">'+
					'<span class="input-group-text" ><span class="far fa-square fa-fw"></span></span>'+
					'</div>';
        	} 
        	
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objPlaceHolder+'" >';
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this, "Checks", function(idElement, objName, objTitle, objRequired, objReadonly, objIcon, objHelp, objOptions){
		objTitle = objTitle || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "check-square";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	
		
		htmlCode += '<div class="input-group-prepend" ">'+
		'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
		'</div>';
		
		if(objReadonly == 0){
						
			htmlCode += 	'<div class="form-control" style="height:auto;">'
				
			var first=true;
			for(var opt in objOptions) {
				if (!first){htmlCode += '<br>';}
				htmlCode += '<input type="checkbox" class="checkbox" id="'+objName+objOptions[opt].id+'" name="'+objName+objOptions[opt].id+'" >';
				htmlCode += ' <label for="'+objName+objOptions[opt].id+'">&nbsp;'+objOptions[opt].text+'</label>';
				first = false;
			}
			
			htmlCode += '</div>';
			
			htmlCode += '<script type="text/javascript">';
			for(var opt in objOptions) {
				htmlCode +=  '$("#'+objName+objOptions[opt].id+'").ejCheckBox({ ';
				if (objOptions[opt].check==1) {
					htmlCode +=  ' checked: true ';
				}
				htmlCode +=  ' });'
			};
			htmlCode +=  ' </script>';
        } else {
        	/*
        	htmlCode += '<input type="text" id="'+objName+'" name="'+objName+'" class="form-control" ';
        	htmlCode += " READONLY ";
        	htmlCode += 'value="'+objPlaceHolder+'" >';
        	*/
        	htmlCode += 	'<div class="form-control" READONLY style="height:auto;">'
				
    			var first=true;
    			for(var opt in objOptions) {
    				if (objOptions[opt].check==1) {
	    				if (!first){htmlCode += '<br>';}
	    				htmlCode += '<input type="checkbox" class="checkbox" id="'+objName+objOptions[opt].id+'" name="'+objName+objOptions[opt].id+'" >';
	    				htmlCode += ' <label for="'+objName+objOptions[opt].id+'">&nbsp;'+objOptions[opt].text+'</label>';
	    				first = false;
    				}
    			}
    			
    			htmlCode += '</div>';
    			
    			htmlCode += '<script type="text/javascript">';
    			for(var opt in objOptions) {
    				if (objOptions[opt].check==1) {
	    				htmlCode +=  '$("#'+objName+objOptions[opt].id+'").ejCheckBox({ ';
	    				htmlCode +=  ' checked: true ';
	    				htmlCode +=  ' });'
    				}
    			};
    			htmlCode +=  ' </script>';
        	
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});

	addMethod(this, "Options", function(idElement, objName, objTitle, objRequired, objReadonly, objIcon, objHelp, objOptions){
		objTitle = objTitle || "";
		objHelp = objHelp || "";
		objIcon = objIcon || "dot-circle";
		
		var htmlCode = '<div class="input-group">';
			
		if(objTitle.length > 0 ){
			htmlCode += 	'<div class="input-group-prepend" style="width:30%">'+
						'<span class="input-group-text" style="width:100%" >'+objTitle;
			if(objRequired == 1 && objReadonly == 0){
				htmlCode += '<span class="fas fa-caret-right fa-fw w_color_required"></span>';
			}
			htmlCode += '</span></div>';
		}	

		htmlCode += '<div class="input-group-prepend" ">'+
			'<span class="input-group-text" ><span class="far fa-'+objIcon+' fa-fw"></span></span>'+
			'</div>';
		
		if(objReadonly == 0){
			htmlCode += 	'<div class="form-control" style="height:auto;">';
			
			var first=true;
			for(var opt in objOptions) {
				if (!first){htmlCode += '<br>';}
				htmlCode += '<input type="radio" class="radioButton" id="'+objName+objOptions[opt].id+'" name="'+objName+'_grp" >';
				htmlCode += ' <label for="'+objName+objOptions[opt].id+'">&nbsp;'+objOptions[opt].text+'</label>';
				first = false;
			}
			
			htmlCode += '</div>';			
			
			htmlCode += '<script type="text/javascript">';

			for(var opt in objOptions) {
				htmlCode +=  '$("#'+objName+objOptions[opt].id+'").ejRadioButton({ ';
				if (+objOptions[opt].check==1) {
					htmlCode +=  ' checked: true ';
				}
				htmlCode +=  ' });';
			}
			
			htmlCode +=  ' </script>';
        } else {   
			for(var opt in objOptions) {
				if (+objOptions[opt].check==1) {
					htmlCode += '<input type="text" id="'+objName+objOptions[opt].id+'" name="'+objName+objOptions[opt].id+'" class="form-control" ';
		        	htmlCode += " READONLY ";
		        	htmlCode += 'value="'+objOptions[opt].text+'" >';
				}
			}        	
        }
		
		if(objHelp.length > 0 ){
			htmlCode += 	'<div class="input-group-append" id="'+objName+'ToolTip">'+
						'<span class="input-group-text">'+
						'<span class="fal fa-question-circle fa-fw"></span></span>'+
						'</div>';
			htmlCode += 	'<script type="text/javascript">';
			htmlCode += 	'$("#'+objName+'ToolTip").ejTooltip({content: "'+objHelp+'", '+
			     		' position :{  stem: { horizontal: "right", vertical: "bottom" }, '+
						' target: { horizontal : "left", vertical : "center" } } });</script>';
			
		}	
		htmlCode += ' </div>';
		// console.log(htmlCode);
		
		$('#' + idElement).append(htmlCode);

	});
	
	addMethod(this,"openButtons", function(idElement, objName, strAlign, strSize){
		
			var varSize = (strSize == "LG") ? "btn-group-lg":"";
			var varAlign = (strAlign == "R") ? "float-right":"float-left";

			var htmlCode = '<div id="'+objName+'" name="'+objName+'" class="btn-group '+varSize+' '+varAlign+'" ';
				htmlCode += '>';
				
			// // console.log(htmlCode);
			$('#' + idElement).append(htmlCode);
		});


	addMethod(this,"itemButton", function(idElement, objName, objTitle, objIcon, objButton, objVisible){
		var varVisible= '';
		if(objVisible === "0"){
			varVisible = ' style="display:none;" ';
		}
		var htmlCode = '<button type="button" class="btn btn-'+objButton+'" id="'+objName+'" name="'+objName+'" '+varVisible+'>'+
		'<i class="far fa-'+objIcon+' fa-fw"></i> '+objTitle+'</button>';
				
		// console.log(htmlCode);
		$('#' + idElement).append(htmlCode);
	});

	addMethod(this,"itemButtonSubmit", function(idElement, objName, objTitle, objIcon, objButton, objVisible){
		var varVisible= '';
		if(objVisible === "0"){
			varVisible = ' style="display:none;" ';
		}
		
		var htmlCode = '<button type="submit" class="btn btn-'+objButton+'" id="'+objName+'" name="'+objName+'" '+varVisible+'>'+
	    '<i class="far fa-'+objIcon+'  fa-fw "></i> ' + objTitle + '</button>'
	    // console.log(htmlCode);
		$('#' + idElement).append(htmlCode);
	});
	
	addMethod(this, "alertDismiss", function(objDiv, alertType,alertContent){
		var html = '<div class="alert '+alertType+' alert-dismissible fade show" role="alert" >' +alertContent;
		// 	'style="position: absolute;right: 1%;z-index: 10000;top: 5%; ">' +alertContent;
		html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
		    '<span aria-hidden="true">&times;</span></button></div>';
		$('#'+objDiv).append(html);
    	$(".alert-dismissible").delay(8000).slideUp('slow', function(){
    			$(this).alert('close');
    	});
	});
	
	addMethod(this, "Modal", function(idElement, objName, objTitle, objImg, objBody, btnTitle, btnType, btnIcon){
		console.log("Modal");
		
		var htmlCode = '<div class="modal fade" style="z-index:100000;" id="'+objName+'" role="dialog">'+
							'<div class="modal-dialog "><div class="modal-content">'+
								'<div class="modal-header">'+
				    				'<h4 class="modal-title"><img src="'+objImg+'" height="30px"/>&nbsp;'+objTitle+' </h4> '+
				    				'<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button></div>'+
				    			'<div class="modal-body">'+objBody+'</div>'+
				    				'<div class="modal-footer">'+
					    				'<button  type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-ban fa-fw w_color_sistem"></i> Cancelar</button>'+
					    				'<a href="#" id="'+objName+'Btn" name="'+objName+'Btn" type="button" class="btn btn-'+btnType+'" '+
					    				'onclick="$(\'#'+objName+'\').modal(\'hide\');" ><i class="fas fa-'+btnIcon+' fa-fw "></i> '+btnTitle+'</a>'+
				    				'</div>'+
			    				'</div>'+
		    				'</div>'+
	    				'</div>';
			
		// console.log(htmlCode);
		$('#' + idElement).append(htmlCode);
		

	});
	
}


function Alerta(){

	addMethod(this, "error", function(title, msg, icon){
		var ic = (icon == '') ? 'exclamation-triangle' : icon;
		var html = '<div class="alert alert-dismissible col-lg-4" style="position: absolute;right: 1%;z-index: 10000;top: 5%; background: #b81111; box-shadow: 0px 0px 13px 0px rgba(138,135,138,1); color: #ffffff;">'+
		'<i class="fas '+ic+'">  </i>'+
    	'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
    	'<strong>'+" "+title+'</strong> '+msg+'.</div>';
    	$('body').append(html);
    	$(".alert-dismissible").fadeIn('slow', function () { $(this).delay(5000).fadeOut('slow', function(){$(this).alert('close');}); });
	});

	addMethod(this, "success", function(title, msg, icon){
		var ic = (icon == '') ? 'check' : icon;
		var html = '<div class="alert alert-dismissible col-lg-4" style="position: absolute;right: 1%;z-index: 10000;top: 5%; background: #E67714; box-shadow: 0px 0px 13px 0px rgba(138,135,138,1); color: #ffffff;">'+
		'<i class="fas '+ic+'"></i>'+
    	'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
    	'<strong>'+" "+title+'</strong> '+msg+'.</div>';
    	$('body').append(html);
    	$(".alert-dismissible").fadeIn('slow', function () { $(this).delay(5000).fadeOut('slow', function(){$(this).alert('close');}); });
	});
	
	

}





function ValidarNumero(e, validaData){
	var keyCode = e.which || e.keyCode;
	var ctrlKey = e.ctrlKey;
	
	if((keyCode < 48) || (keyCode > 57)){
		e.preventDefault();
	}
}

function ValidarDecimal(e, validaData){
	var keyCode = e.which || e.keyCode;
	var ctrlKey = e.ctrlKey;
	
	if((keyCode != 46) && (keyCode < 48) || (keyCode > 57) ){
		e.preventDefault();
	}else{
		var num = validaData + String.fromCharCode(keyCode);
		
		if(validaData > 0 && !parseFloat(+num)){
			e.preventDefault();
		}
	}
}

function CheckFloat(objName){
	var strObj = document.getElementById(objName);
	var floatPath = /^-?\d*(\.\d+)$/;
	
	if(!floatPath.test(strObj.value)){
		strObj.value = 0;
	}
}

function ValidarEmail(e, validaData){
	var keyCode = e.which || e.keyCode;
	var ctrlKey = e.ctrlKey;
	
	if((keyCode != 46) && (keyCode != 45) && (keyCode != 64) && (keyCode != 95) && ((keyCode < 97) || (keyCode > 122)) && ((keyCode < 48) || (keyCode > 57))){
		e.preventDefault();
	}
}

function CheckEmail(objName){
	var strObj = document.getElementById(objName);
	var mailPath = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	if(!mailPath.test(strObj.value)){
		strObj.value = "";
	}
}


function ValidarNoEscribir(e, validaData){
	
	e.preventDefault();

		
}

function inputVacio(objName)
{ 
	var valor = document.getElementById(objName);
	console.log("valor input "+valor);
	if(valor == '')alert('esta vacio')
	}

function validaForm(form)
{ 
	console.log("validaForm ");
	evObject.preventDefault();
	var todoCorrecto = true;
	var formulario = document.formularioContacto;
	for (var i=0; i<formulario.length; i++) {

	    if(formulario[i].type =='text') {
	
		   if (formulario[i].value == null || formulario[i].value.length == 0 || /^\s*$/.test(formulario[i].value)){
		
		   alert (formulario[i].name+ ' no puede estar vacío o contener sólo espacios en blanco');
		
		   todoCorrecto=false;
		
		   }	
	    }
	}
	if (todoCorrecto ==true) {formulario.submit();}

}






