
	//GLOBALS
    var w3c = (document.getElementById) ? 1:0
    var ns4 = (document.layers) ? 1:0  //browser detect for NS4 & W3C standards

	// returns an object reference.
	function getObject(obj) {
		if (w3c)
			var theObj = document.getElementById(obj);
		else
			if (ns4)
				var theObj = eval("document." + obj);
		return theObj;
	}
    
  // Date format moment locale
    moment.locale('es');
	
  
  //Customize your Notification  
    const configRight = {
        timeout: 5000,
        positionY: "top", // top or bottom
        positionX: "right", // right left, center
        distanceY: 60, // Integer value
        distanceX: 20, // Integer value
        zIndex: 100, // Integer value
        theme: "default" // default, ligh or  dark (leave empty for "default" theme)
    };

    //Create a new Toastmejs class instance
    const showToast = new Toastme(configRight);
    
    const configCenter = {
            timeout: 5000,
            positionY: "top", // top or bottom
            positionX: "center", // right left, center
            distanceY: 60, // Integer value
            distanceX: 0, // Integer value
            zIndex: 100, // Integer value
            theme: "default" // default, ligh or  dark (leave empty for "default" theme)
        };

        //Create a new Toastmejs class instance
        const centerToast = new Toastme(configCenter);
    
        
        // USAGE
        /*
         * showToast.default("This is a 'default' notification");
			showToast.success("This is a 'success' notification");
			centerToast.error("This is an 'error' notification");
			showToast.warning("This is a 'warning' notification");
			centerToast.info("This is an 'info' notification");
         */

        // Block Obj Global Variable
        const blockObj = new block();

function confirmPost(preguntaStr)
{
var agree=confirm(preguntaStr);
if (agree)
return true ;
else
return false ;
}

  // VALIDATE
    function Valida(e, validatype, validadata){
        var keyCode =  e.which || e.keyCode;    
        var ctrlKey =  e.ctrlKey;    
        // Allow: backspace, delete, tab, escape, enter      
        if ($.inArray(e.keyCode, [8, 27, 9, 13]) !== -1 ||
            // Allow: Ctrl+A
            (keyCode == 65 && ctrlKey === true) ||
            // Allow: Ctrl+C
            (keyCode == 67 && ctrlKey === true) ||
            // Allow: Ctrl+X
            (keyCode == 88 && ctrlKey === true) ||
            // Allow: home, end, left, right
            (keyCode >= 35 && keyCode <= 39) ) {
            return;
        }

		// alert(keyCode);
        switch (validatype) {
        case "number": // 0..9 sin punto
            if ((keyCode < 48) || (keyCode > 57)) 
                e.preventDefault();
            break;
        case "decimal": // 0..9 y con punto            
            if ((keyCode != 46) && (keyCode < 48) || (keyCode > 57) ) {
                    e.preventDefault();
                } else {
                    var num = validadata + String.fromCharCode(keyCode);
                    // alert(parseFloat(+num));
                    if (validadata>0 &&!parseFloat(+num)) {
                        e.preventDefault();  
                    }
                }
            break;        
        case "date": // 0..9 y diagonal FECHAS 
            if ((keyCode < 47) || (keyCode > 57))  {
                     e.preventDefault();               
                }
            break;
        case "email": // a..zZ..Z0..9.-_@ para email
            // alert(keyCode);
            if ((keyCode != 46) && (keyCode != 45) && (keyCode != 64) && (keyCode != 95) && ((keyCode < 97) || (keyCode > 122)) && ((keyCode < 48) || (keyCode > 57)))
                e.preventDefault();
            break;
        case "letters": // a..zZ..Z sin espacio
            if ((keyCode < 65) || (keyCode > 90) && (keyCode < 97) || (keyCode > 122)) 
               e.preventDefault();
            break;
        case "letterspace": // a..zZ..Z con espacios
            if ((keyCode != 32) && (keyCode < 65) || (keyCode > 90) && (keyCode < 97) || (keyCode > 122)) 
                e.preventDefault();
            break;
        case "allnospace": // todo sin espacios
            if ((keyCode == 32)) 
                e.preventDefault();
            break;
        }
    }
 
    
function CheckDate(objfecha) {
    var datestr = getObject(objfecha);
    // alert("FECHA : "+datestr.value+" ES FECHA " + isDate(datestr.value) );
    if (!isDate(datestr.value)) {
        var now = new Date();
        //datestr.value=now.getDate()+'/'+now.getMonth()+1+'/'+now.getFullYear();
        datestr.value = '';
    } 
}

function CheckEmail(objemail) {
    var emailstr = getObject(objemail);
    // alert("EMAIL : "+emailstr.value+" ES EMAIL " + isEmail(emailstr.value));
    if (!isEmail(emailstr.value)) {
       emailstr.value = '';
    } 
}

function CheckFloat(objnum) {
    var numstr = getObject(objnum);
    if (!numstr.value>0 && !parseFloat(+numstr.value)) {        
       numstr.value = 0;
    }
}

function CheckInt(objnum) {
    var numstr = getObject(objnum);
    if (!numstr.value>0 || !parseFloat(+numstr.value)) {        
       numstr.value = 0;
    } 
	
}

function isEmail(mailStr) {
    var mailPat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return mailPat.test(mailStr);
}

function isDate(dateStr) { 

    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
    var matchArray = dateStr.match(datePat); 

    if (matchArray == null) { 
        // alert("Please enter date as either mm/dd/yyyy or mm-dd-yyyy."); 
        return false; 
    } 
    
    day = matchArray[1]; 
    month = matchArray[3]; 
    year = matchArray[5]; 

    if (month < 1 || month > 12) { 
        // alert("Month must be between 1 and 12."); 
        return false; 
    } 

    if (day < 1 || day > 31) { 
        // alert("Day must be between 1 and 31."); 
        return false; 
    } 

    if ((month==4 || month==6 || month==9 || month==11) && day==31) { 
        // alert("Month "+month+" doesn't have 31 days!") 
        return false; 
    } 

    if (month == 2) { 
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)); 
        if (day > 29 || (day==29 && !isleap)) { 
            // alert("February " + year + " doesn't have " + day + " days!"); 
            return false; 
        } 
    } 
    return true; 
} 

function printDiv(divName) 
{
  var divToPrint=document.getElementById(divName);

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  // setTimeout(function(){newWin.close();},10);

}

// Months Array
var monthArr = {
		1:'Enero',
		2:'Febrero',
		3:'Marzo',
		4:'Abril',
		5:'Mayo',
		6:'Junio',
		7:'Julio',
		8:'Agosto',
		9:'Septiembre',
		10:'Octubre',
		11:'Noviembre',
		12:'Diciembre'};