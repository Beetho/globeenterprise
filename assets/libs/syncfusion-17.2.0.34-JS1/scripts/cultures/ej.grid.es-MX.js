// JavaScript Document

ej.Grid.Locale["es-MX"] = {
	EmptyRecord: "&nbsp;No hay registros que mostrar",
	GroupDropArea: "Arrastre un encabezado de columna aquí",
	DeleteOperationAlert: "No hay registros seleccionados para la operación de eliminación",
	EditOperationAlert: "No hay registros seleccionados para la operación de edición",
	SaveButton: "guardar",
	CancelButton: "cancelar",
	EditFormTitle: "Editar detalles de",
	GroupCaptionFormat: "{{:headerText}}: {{:key}} - {{:count}} {{if count == 1}}registro {{else}}registros{{/if}}",
	UnGroup: "Haga clic aquí para desagrupar"
};
ej.Pager.Locale["es-MX"] = {
	pagerInfo: "{0} de {1} páginas ({2} registros)",
	firstPageTooltip: "Ir a la primera página",
	lastPageTooltip: "Ir a la última página",
	nextPageTooltip: "Ir a la página siguiente",
	previousPageTooltip: "Ir a la página anterior",
	nextPagerTooltip: "Ir al siguiente localizador",
	previousPagerTooltip: "Ir a localizador anterior"
};


ej.ExcelFilter.Locale["es-MX"] = {

	StringMenuOptions 	:[{ text:"Igual",value:"equal"},{ text:"Not Equal", value:"notequal"},{ text:"Starts With",value:"startswith"}, { text:"Ends With",value:"endswith"},{ text:"Contains",value:"contains"}, {text:"Custom Filter", value:"customfilter"}],
	NumberMenuOptions 	:[{text:"Igual",value:"equal"}, {text:"Not Equal",value:"notequal"}, { text:"Less Than",value:"lessthan"}, {text:"Less Than Or Equal", value:"lessthanorequal"}, {text:"Greater Than",value:"greaterthan"},{ text:"Greater Than Or Equal", value:"greaterthanorequal"}, { text:"Between",value:"between"},{ text:"Custom Filter", value:"customfilter"}],
	DateMenuOptions   	:[{ text:"Igual", value:"equal"}, {text:"Not Equal",value:"notequal"},{text:"Less Than",value:"lessthan"}, {text:"Less Than Or Equal",value:"lessthanorequal"}, {text:"Greater Than",value:"greaterthan"},{text:"Greater Than Or Equal", value:"greaterthanorequal"}, { text:"Between",value:"between"},{ text:"Custom Filter", value:"customfilter"}],
	DatetimeMenuOptions :[{ text: "Igual", value: "equal" }, { text: "Not Equal", value: "notequal" }, { text: "Less Than", value: "lessthan" }, { text: "Less Than Or Equal", value: "lessthanorequal" }, { text: "Greater Than", value: "greaterthan" }, { text: "Greater Than Or Equal", value: "greaterthanorequal" }, { text: "Between", value: "between" }, { text: "Custom Filter", value: "customfilter" }]


};