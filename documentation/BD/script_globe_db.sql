
CREATE SCHEMA IF NOT EXISTS globe_db DEFAULT CHARACTER SET utf8;


USE globe_db;

CREATE TABLE IF NOT EXISTS globe (
  id_globe_register INT(11) NOT NULL AUTO_INCREMENT,
  size_globe float NOT NULL ,
  date_register DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_globe_register`));