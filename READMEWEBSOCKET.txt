Configuración con Web Socket

Habilitar IP y puerto:
	En la consola de comandos de windows, ubicarnos en la raíz del proyecto 
	y ejecutar lo siguiente: php server.php
	Dentro de este archivo es donde esta configurado por defecto 192.168.0.5:9300
	
	Archivos a modificar de acuerdo a su IP: 
		Globe/config/development.cfg
		Globe/server.php
		

Pantallas en el navegador:
	Para visualizar la pantalla donde ingresaremos el tamaño de nuestro Globo
	escribimos en nuestro navegador: http://localhost/Globe/wsconfig/globe ó http://192.168.0.5/Globe/wsconfig/globe

	Para visualizar la pantalla donde se muestra nuestro Globo
	escribimos en nuestro navegador: http://localhost/Globe/wsglobe ó http://192.168.0.5/Globe/wsglobe
	
	Nota: cambiar la IP por la de usted.